# British Library themes for RemarkJS presentations

These are  unofficial stylesheets
replicating the appearance of [The British Library](https://bl.uk)'s official PowerPoint template
for use with [Remark](https://remarkjs.com/).
The pre-2021 theme's name, "Boston Spa",
is taken from the location of the Library's main storage site in Yorkshire,
originally the site of the National Lending Library for Science and Technology.
The new theme's name, "Walton",
is taken from the village closest to the Library's Yorkshire site.

Remark is:

> A simple, in-browser, markdown-driven slideshow tool targeted at people who know their way around HTML and CSS

## How to use

In short,
download/clone this repository
then create a HTML file with the following content
(you may need to update the `<link>` tag on line 6
to refer to the location of the CSS file on your computer or website)
and write your slides as HTML in the `textarea`:

``` html
<!DOCTYPE html>
<html>
  <head>
    <title>Title</title>
    <meta charset="utf-8">
    <link href="walton.css" rel="stylesheet">
    <style>
      /* custom styles here */
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# Title

---

# Agenda

1. Introduction
2. Deep-dive
3. ...

---

# Introduction

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
```

For more detailed example of how to use the theme,
look at <template.html> in this repsitory.

For full information about the capabilities of remark,
visit <https://remarkjs.com/>.

## Copyright and licensing

The Library has various Domain, text, and protected marks
registered in certain jurisdictions,
including the British Library logo used in this theme.
No use of these or any other trade marks of the British Library
may be made by you
except for the purpose of referring to the Library
or its associated brands lawfully and in good faith only.
If you would like to license our material,
please see our [Brand Licensing](https://www.bl.uk/about-us/work-with-us/brand-and-image-licensing) page.

The image used on the final "Thanks" slide of the Boston Spa theme is
by [Courtney Hedger](https://unsplash.com/photos/t48eHCSCnds)
on [Unsplash](https://unsplash.com/),
used under the [Unsplash License](https://unsplash.com/license).
